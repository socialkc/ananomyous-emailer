//This will be nice for receiving feedback from other application.
//But i don't work for ananomious email.

package com.example.ananomail;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.*;

public class MainActivity extends Activity {
	Button btnSendOrgi;
Button btnSend;
EditText txtTo;
EditText txtFrom;
EditText txtSubject;
EditText txtMessage;
Intent emailIntent;
HttpClient Client;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	btnSendOrgi = (Button)findViewById(R.id.sendOrgi);
	btnSend = (Button)findViewById(R.id.send);
	txtTo = (EditText)findViewById(R.id.to);
	txtFrom = (EditText)findViewById(R.id.from);
	txtSubject = (EditText)findViewById(R.id.subject);
	txtMessage = (EditText)findViewById(R.id.message);
	Client = new DefaultHttpClient();
	
	btnSend.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
		
		String URL = "http://bishnu.comli.com/send.php?from=";
		URL += space2Plus(txtFrom.getText().toString());
		URL += "&to=";
		URL += space2Plus(txtTo.getText().toString());
		URL += "&subject=";
		URL += space2Plus(txtSubject.getText().toString());
		URL += "&message=";
		URL += space2Plus(txtMessage.getText().toString());
		URL += "&send=Send+Email";
		
		HttpGetReq reqSend = new HttpGetReq(URL);
		reqSend.start();
		
		//String URL2 = "http://bishnu.comli.com/send.php?from=Sender&to=bidaribishnu7%40gmail.com&subject=Suject+Created&message=Message+body+created&send=Send+Email";
/*		try{
			HttpGet httpget= new HttpGet(URL);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String setServerRes = Client.execute(httpget,responseHandler);
			
			Toast.makeText(getApplicationContext(), setServerRes, Toast.LENGTH_SHORT).show();
		
		}catch(Exception error){
			Toast.makeText(getApplicationContext(), "Error Encountered.. ", Toast.LENGTH_SHORT).show();	
		}
		*/
		
		
		}
	});
	
	btnSendOrgi.setOnClickListener(new View.OnClickListener(){
		@Override
		public void onClick(View v){
			emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",txtTo.getText().toString() ,null));
			emailIntent.putExtra(Intent.EXTRA_EMAIL,txtTo.getText().toString());
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, txtSubject.getText().toString());
			emailIntent.putExtra(Intent.EXTRA_TEXT, txtMessage.getText().toString());
			startActivity(Intent.createChooser(emailIntent, "Send Email .. "));
			
		}
	});
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
private String space2Plus(String orgi){
	int len = orgi.length();
	String spaceRemoved="";
	
//	#   $   %   &    +   ?  ;   :    ,  / 
//
//	%23 %24 %25 %26 %2B %3F %3B %3A %2C %2F
//	
	for(int i=0;i<len;i++)
	{
		if(orgi.charAt(i)==' ')
			spaceRemoved += "+";
		else if(orgi.charAt(i)=='@')
			spaceRemoved += "%40";
		else if(orgi.charAt(i)=='#')
			spaceRemoved += "%23";
		else if(orgi.charAt(i)=='$')
			spaceRemoved += "%24";
		else if(orgi.charAt(i)=='%')
			spaceRemoved += "%25";
		else if(orgi.charAt(i)=='&')
			spaceRemoved += "%26";
		else if(orgi.charAt(i)=='+')
			spaceRemoved += "%2B";
		else if(orgi.charAt(i)=='?')
			spaceRemoved += "%3F";
		else if(orgi.charAt(i)==';')
			spaceRemoved += "%3B";
		else if(orgi.charAt(i)==':')
			spaceRemoved += "%3A";
		else if(orgi.charAt(i)==',')
			spaceRemoved += "%2C";
		else if(orgi.charAt(i)=='/')
			spaceRemoved += "%2F";
		else
			spaceRemoved += orgi.charAt(i);
	}
	return spaceRemoved;
}
}
class HttpGetReq extends Thread{
	HttpClient Client;
	private String URL;
	public HttpGetReq(String URL){
		this.URL = URL;
		Client = new DefaultHttpClient();
	}
	public void run(){
		  Toast.makeText(HttpGetReq, "STarting", Toast.LENGTH_SHORT).show();
		try{
			HttpGet httpget = new HttpGet(URL);
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String setServerRes = Client.execute(httpget,responseHandler);
			
			Toast.makeText(null,"Success",Toast.LENGTH_SHORT).show();
			
			if( setServerRes.startsWith("Message Sent Successfully")){
				Toast.makeText(null,"Success",Toast.LENGTH_SHORT).show();
			}
			else {
				throw new Exception("Error Sending mail");
			}
		}catch(Exception err){
			Toast.makeText(null,"Error ::: "+err.getMessage(),Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	          
	        }
	    });
	}
}
